# phonebook-client

A client library for accessing the [Phonebook service][], generated with
[openapi-python-client][].

Installation with [Pipenv][]:

```bash
pipenv install 'phonebook-client @ git+https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/phonebook-client-python.git'
```

To update the client, run this command in its parent directory:

```bash
openapi-python-client update --url https://phonebook.primary.secauto-live.sec.gitlab.net/openapi.json
```

[openapi-python-client]: https://github.com/openapi-generators/openapi-python-client
[phonebook service]: https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/phonebook
[pipenv]: https://pipenv.pypa.io/

## Usage

First, create a client:

```python
from phonebook_client import Client

client = Client(base_url="https://api.example.com")
```

If the endpoints you're going to hit require authentication, use `AuthenticatedClient` instead:

```python
from phonebook_client import AuthenticatedClient

client = AuthenticatedClient(base_url="https://api.example.com", token="SuperSecretToken")
```

Now call your endpoint and use your models:

```python
from phonebook_client.api.users import get_user
from phonebook_client.models import User, UserKey
from phonebook_client.types import Response

user: User = get_user.sync(client=client, k=UserKey.GITLAB_USERNAME, v='username')
# or if you need more info (e.g. status_code)
response: Response[User] = get_user.sync_detailed(client=client, k=UserKey.GITLAB_USERNAME, v='username')
```

Or do the same thing with an async version:

```python
from phonebook_client.api.users import get_user
from phonebook_client.models import User
from phonebook_client.types import Response

user: User = await get_user.asyncio(client=client, k=UserKey.GITLAB_USERNAME, v='username')
# or if you need more info (e.g. status_code)
response: Response[User] = await get_user.asyncio_detailed(client=client, k=UserKey.GITLAB_USERNAME, v='username')
```

Things to know:

1. Every path/method combo becomes a Python module with four functions:
    1. `sync`: Blocking request that returns parsed data (if successful) or `None`
    1. `sync_detailed`: Blocking request that always returns a `Request`, optionally with `parsed` set if the request was successful.
    1. `asyncio`: Like `sync` but the async instead of blocking
    1. `asyncio_detailed`: Like `sync_detailed` by async instead of blocking
1. All path/query params, and bodies become method arguments.
1. If your endpoint had any tags on it, the first tag will be used as a module name for the function (`users` above)
1. Any endpoint which did not have a tag will be in `phonebook_client.api.default`
