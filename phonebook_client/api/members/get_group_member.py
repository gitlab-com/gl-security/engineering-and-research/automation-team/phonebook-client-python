from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import Client
from ...models.http_validation_error import HTTPValidationError
from ...models.member import Member
from ...types import UNSET, Response, Unset


def _get_kwargs(
    group_id: int,
    user_id: int,
    *,
    client: Client,
    instance: Union[Unset, None, Any] = UNSET,
    api_token: Union[Unset, None, Any] = UNSET,
) -> Dict[str, Any]:
    url = "{}/groups/{group_id}/members/{user_id}".format(client.base_url, group_id=group_id, user_id=user_id)

    headers: Dict[str, str] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    params: Dict[str, Any] = {}
    params["instance"] = instance

    params["api_token"] = api_token

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    return {
        "method": "get",
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "params": params,
    }


def _parse_response(*, client: Client, response: httpx.Response) -> Optional[Union[HTTPValidationError, Member]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = Member.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY:
        response_422 = HTTPValidationError.from_dict(response.json())

        return response_422
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(f"Unexpected status code: {response.status_code}")
    else:
        return None


def _build_response(*, client: Client, response: httpx.Response) -> Response[Union[HTTPValidationError, Member]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    group_id: int,
    user_id: int,
    *,
    client: Client,
    instance: Union[Unset, None, Any] = UNSET,
    api_token: Union[Unset, None, Any] = UNSET,
) -> Response[Union[HTTPValidationError, Member]]:
    """Get Group Member

     Return the GitLab group member.

    <https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project>

    Args:
        group_id (int):
        user_id (int):
        instance (Union[Unset, None, Any]):
        api_token (Union[Unset, None, Any]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[HTTPValidationError, Member]]
    """

    kwargs = _get_kwargs(
        group_id=group_id,
        user_id=user_id,
        client=client,
        instance=instance,
        api_token=api_token,
    )

    response = httpx.request(
        verify=client.verify_ssl,
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    group_id: int,
    user_id: int,
    *,
    client: Client,
    instance: Union[Unset, None, Any] = UNSET,
    api_token: Union[Unset, None, Any] = UNSET,
) -> Optional[Union[HTTPValidationError, Member]]:
    """Get Group Member

     Return the GitLab group member.

    <https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project>

    Args:
        group_id (int):
        user_id (int):
        instance (Union[Unset, None, Any]):
        api_token (Union[Unset, None, Any]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[HTTPValidationError, Member]]
    """

    return sync_detailed(
        group_id=group_id,
        user_id=user_id,
        client=client,
        instance=instance,
        api_token=api_token,
    ).parsed


async def asyncio_detailed(
    group_id: int,
    user_id: int,
    *,
    client: Client,
    instance: Union[Unset, None, Any] = UNSET,
    api_token: Union[Unset, None, Any] = UNSET,
) -> Response[Union[HTTPValidationError, Member]]:
    """Get Group Member

     Return the GitLab group member.

    <https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project>

    Args:
        group_id (int):
        user_id (int):
        instance (Union[Unset, None, Any]):
        api_token (Union[Unset, None, Any]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[HTTPValidationError, Member]]
    """

    kwargs = _get_kwargs(
        group_id=group_id,
        user_id=user_id,
        client=client,
        instance=instance,
        api_token=api_token,
    )

    async with httpx.AsyncClient(verify=client.verify_ssl) as _client:
        response = await _client.request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    group_id: int,
    user_id: int,
    *,
    client: Client,
    instance: Union[Unset, None, Any] = UNSET,
    api_token: Union[Unset, None, Any] = UNSET,
) -> Optional[Union[HTTPValidationError, Member]]:
    """Get Group Member

     Return the GitLab group member.

    <https://docs.gitlab.com/ee/api/members.html#get-a-member-of-a-group-or-project>

    Args:
        group_id (int):
        user_id (int):
        instance (Union[Unset, None, Any]):
        api_token (Union[Unset, None, Any]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[HTTPValidationError, Member]]
    """

    return (
        await asyncio_detailed(
            group_id=group_id,
            user_id=user_id,
            client=client,
            instance=instance,
            api_token=api_token,
        )
    ).parsed
