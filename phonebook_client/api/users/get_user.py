from http import HTTPStatus
from typing import Any, Dict, Optional, Union, cast

import httpx

from ... import errors
from ...client import Client
from ...models.http_validation_error import HTTPValidationError
from ...models.user import User
from ...models.user_key import UserKey
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    client: Client,
    k: UserKey,
    v: str,
    api_token: Union[Unset, None, Any] = UNSET,
    instance: Union[Unset, None, Any] = UNSET,
) -> Dict[str, Any]:
    url = "{}/user".format(client.base_url)

    headers: Dict[str, str] = client.get_headers()
    cookies: Dict[str, Any] = client.get_cookies()

    params: Dict[str, Any] = {}
    json_k = k.value

    params["k"] = json_k

    params["v"] = v

    params["api_token"] = api_token

    params["instance"] = instance

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    return {
        "method": "get",
        "url": url,
        "headers": headers,
        "cookies": cookies,
        "timeout": client.get_timeout(),
        "params": params,
    }


def _parse_response(*, client: Client, response: httpx.Response) -> Optional[Union[Any, HTTPValidationError, User]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = User.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = cast(Any, None)
        return response_404
    if response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY:
        response_422 = HTTPValidationError.from_dict(response.json())

        return response_422
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(f"Unexpected status code: {response.status_code}")
    else:
        return None


def _build_response(*, client: Client, response: httpx.Response) -> Response[Union[Any, HTTPValidationError, User]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Client,
    k: UserKey,
    v: str,
    api_token: Union[Unset, None, Any] = UNSET,
    instance: Union[Unset, None, Any] = UNSET,
) -> Response[Union[Any, HTTPValidationError, User]]:
    """Get User

     Return the single matching user for `k` and `v`.

    Args:
        k (UserKey): Valid user search keys.
        v (str):
        api_token (Union[Unset, None, Any]):
        instance (Union[Unset, None, Any]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, HTTPValidationError, User]]
    """

    kwargs = _get_kwargs(
        client=client,
        k=k,
        v=v,
        api_token=api_token,
        instance=instance,
    )

    response = httpx.request(
        verify=client.verify_ssl,
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Client,
    k: UserKey,
    v: str,
    api_token: Union[Unset, None, Any] = UNSET,
    instance: Union[Unset, None, Any] = UNSET,
) -> Optional[Union[Any, HTTPValidationError, User]]:
    """Get User

     Return the single matching user for `k` and `v`.

    Args:
        k (UserKey): Valid user search keys.
        v (str):
        api_token (Union[Unset, None, Any]):
        instance (Union[Unset, None, Any]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, HTTPValidationError, User]]
    """

    return sync_detailed(
        client=client,
        k=k,
        v=v,
        api_token=api_token,
        instance=instance,
    ).parsed


async def asyncio_detailed(
    *,
    client: Client,
    k: UserKey,
    v: str,
    api_token: Union[Unset, None, Any] = UNSET,
    instance: Union[Unset, None, Any] = UNSET,
) -> Response[Union[Any, HTTPValidationError, User]]:
    """Get User

     Return the single matching user for `k` and `v`.

    Args:
        k (UserKey): Valid user search keys.
        v (str):
        api_token (Union[Unset, None, Any]):
        instance (Union[Unset, None, Any]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, HTTPValidationError, User]]
    """

    kwargs = _get_kwargs(
        client=client,
        k=k,
        v=v,
        api_token=api_token,
        instance=instance,
    )

    async with httpx.AsyncClient(verify=client.verify_ssl) as _client:
        response = await _client.request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Client,
    k: UserKey,
    v: str,
    api_token: Union[Unset, None, Any] = UNSET,
    instance: Union[Unset, None, Any] = UNSET,
) -> Optional[Union[Any, HTTPValidationError, User]]:
    """Get User

     Return the single matching user for `k` and `v`.

    Args:
        k (UserKey): Valid user search keys.
        v (str):
        api_token (Union[Unset, None, Any]):
        instance (Union[Unset, None, Any]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, HTTPValidationError, User]]
    """

    return (
        await asyncio_detailed(
            client=client,
            k=k,
            v=v,
            api_token=api_token,
            instance=instance,
        )
    ).parsed
