from typing import Any, Dict, List, Type, TypeVar

import attr

from ..models.access_level import AccessLevel

T = TypeVar("T", bound="Member")


@attr.s(auto_attribs=True)
class Member:
    """GitLab group/project member information.

    <https://docs.gitlab.com/ee/api/members.html>

        Attributes:
            id (int):
            username (str):
            name (str):
            access_level (AccessLevel): GitLab group/project access levels.

                <https://docs.gitlab.com/ee/api/members.html#valid-access-levels>
            state (str):
    """

    id: int
    username: str
    name: str
    access_level: AccessLevel
    state: str
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id
        username = self.username
        name = self.name
        access_level = self.access_level.value

        state = self.state

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "username": username,
                "name": name,
                "access_level": access_level,
                "state": state,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        username = d.pop("username")

        name = d.pop("name")

        access_level = AccessLevel(d.pop("access_level"))

        state = d.pop("state")

        member = cls(
            id=id,
            username=username,
            name=name,
            access_level=access_level,
            state=state,
        )

        member.additional_properties = d
        return member

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
