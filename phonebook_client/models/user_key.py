from enum import Enum


class UserKey(str, Enum):
    DEV_GITLAB_ID = "dev_gitlab_id"
    DEV_GITLAB_USERNAME = "dev_gitlab_username"
    DISPLAY_NAME = "display_name"
    EMAIL = "email"
    EMPLOYEE_NUMBER = "employee_number"
    GITLAB_ID = "gitlab_id"
    GITLAB_USERNAME = "gitlab_username"
    H1_ID = "h1_id"
    H1_USERNAME = "h1_username"
    OKTA_ID = "okta_id"
    OPS_GITLAB_ID = "ops_gitlab_id"
    OPS_GITLAB_USERNAME = "ops_gitlab_username"
    PD_ID = "pd_id"
    SLACK_ID = "slack_id"
    ZENGRC_ID = "zengrc_id"

    def __str__(self) -> str:
        return str(self.value)
