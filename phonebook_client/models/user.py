from typing import Any, Dict, List, Type, TypeVar, Union

import attr

from ..types import UNSET, Unset

T = TypeVar("T", bound="User")


@attr.s(auto_attribs=True)
class User:
    """User information.

    Attributes:
        email (str):
        dev_gitlab_id (Union[Unset, int]):
        dev_gitlab_username (Union[Unset, str]):
        display_name (Union[Unset, str]):
        employee_number (Union[Unset, int]):
        gitlab_id (Union[Unset, int]):
        gitlab_username (Union[Unset, str]):
        h1_id (Union[Unset, str]):
        h1_username (Union[Unset, str]):
        manager_id (Union[Unset, int]):
        okta_id (Union[Unset, str]):
        ops_gitlab_id (Union[Unset, int]):
        ops_gitlab_username (Union[Unset, str]):
        pd_id (Union[Unset, str]):
        slack_id (Union[Unset, str]):
        zengrc_id (Union[Unset, int]):
    """

    email: str
    dev_gitlab_id: Union[Unset, int] = UNSET
    dev_gitlab_username: Union[Unset, str] = UNSET
    display_name: Union[Unset, str] = UNSET
    employee_number: Union[Unset, int] = UNSET
    gitlab_id: Union[Unset, int] = UNSET
    gitlab_username: Union[Unset, str] = UNSET
    h1_id: Union[Unset, str] = UNSET
    h1_username: Union[Unset, str] = UNSET
    manager_id: Union[Unset, int] = UNSET
    okta_id: Union[Unset, str] = UNSET
    ops_gitlab_id: Union[Unset, int] = UNSET
    ops_gitlab_username: Union[Unset, str] = UNSET
    pd_id: Union[Unset, str] = UNSET
    slack_id: Union[Unset, str] = UNSET
    zengrc_id: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = attr.ib(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        email = self.email
        dev_gitlab_id = self.dev_gitlab_id
        dev_gitlab_username = self.dev_gitlab_username
        display_name = self.display_name
        employee_number = self.employee_number
        gitlab_id = self.gitlab_id
        gitlab_username = self.gitlab_username
        h1_id = self.h1_id
        h1_username = self.h1_username
        manager_id = self.manager_id
        okta_id = self.okta_id
        ops_gitlab_id = self.ops_gitlab_id
        ops_gitlab_username = self.ops_gitlab_username
        pd_id = self.pd_id
        slack_id = self.slack_id
        zengrc_id = self.zengrc_id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "email": email,
            }
        )
        if dev_gitlab_id is not UNSET:
            field_dict["dev_gitlab_id"] = dev_gitlab_id
        if dev_gitlab_username is not UNSET:
            field_dict["dev_gitlab_username"] = dev_gitlab_username
        if display_name is not UNSET:
            field_dict["display_name"] = display_name
        if employee_number is not UNSET:
            field_dict["employee_number"] = employee_number
        if gitlab_id is not UNSET:
            field_dict["gitlab_id"] = gitlab_id
        if gitlab_username is not UNSET:
            field_dict["gitlab_username"] = gitlab_username
        if h1_id is not UNSET:
            field_dict["h1_id"] = h1_id
        if h1_username is not UNSET:
            field_dict["h1_username"] = h1_username
        if manager_id is not UNSET:
            field_dict["manager_id"] = manager_id
        if okta_id is not UNSET:
            field_dict["okta_id"] = okta_id
        if ops_gitlab_id is not UNSET:
            field_dict["ops_gitlab_id"] = ops_gitlab_id
        if ops_gitlab_username is not UNSET:
            field_dict["ops_gitlab_username"] = ops_gitlab_username
        if pd_id is not UNSET:
            field_dict["pd_id"] = pd_id
        if slack_id is not UNSET:
            field_dict["slack_id"] = slack_id
        if zengrc_id is not UNSET:
            field_dict["zengrc_id"] = zengrc_id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        email = d.pop("email")

        dev_gitlab_id = d.pop("dev_gitlab_id", UNSET)

        dev_gitlab_username = d.pop("dev_gitlab_username", UNSET)

        display_name = d.pop("display_name", UNSET)

        employee_number = d.pop("employee_number", UNSET)

        gitlab_id = d.pop("gitlab_id", UNSET)

        gitlab_username = d.pop("gitlab_username", UNSET)

        h1_id = d.pop("h1_id", UNSET)

        h1_username = d.pop("h1_username", UNSET)

        manager_id = d.pop("manager_id", UNSET)

        okta_id = d.pop("okta_id", UNSET)

        ops_gitlab_id = d.pop("ops_gitlab_id", UNSET)

        ops_gitlab_username = d.pop("ops_gitlab_username", UNSET)

        pd_id = d.pop("pd_id", UNSET)

        slack_id = d.pop("slack_id", UNSET)

        zengrc_id = d.pop("zengrc_id", UNSET)

        user = cls(
            email=email,
            dev_gitlab_id=dev_gitlab_id,
            dev_gitlab_username=dev_gitlab_username,
            display_name=display_name,
            employee_number=employee_number,
            gitlab_id=gitlab_id,
            gitlab_username=gitlab_username,
            h1_id=h1_id,
            h1_username=h1_username,
            manager_id=manager_id,
            okta_id=okta_id,
            ops_gitlab_id=ops_gitlab_id,
            ops_gitlab_username=ops_gitlab_username,
            pd_id=pd_id,
            slack_id=slack_id,
            zengrc_id=zengrc_id,
        )

        user.additional_properties = d
        return user

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
